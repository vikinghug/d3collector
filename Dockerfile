FROM itsthatguy/ruby-node

# Postgres & Nokogiri
RUN apt-get update -qq && \
    apt-get install -y build-essential libpq-dev \
                       libxml2-dev libxslt1-dev

# setup our shell with some aliases
COPY ./containers/bashrc /root/.bashrc

# Node dependencies
##  We do the npm install in the /tmp/ directory because /code/ is a shared
##  volume. Shared volumes have very slow write speeds, within docker.
##  This significantly reduces the install time (from ~20 minutes to ~5 minutes)
ENV PATH node_modules/.bin:$PATH

RUN mkdir -p /code
WORKDIR /code
COPY package.json /code/
COPY bower.json /code/

RUN npm install --loglevel warn && \
    ./node_modules/.bin/bower install --allow-root

# Rubies
ENV BUNDLE_PATH /ruby_gems
COPY Gemfile* /code/
RUN bundle install --jobs=4 --retry=3

ADD . /code

EXPOSE 3000

CMD ['./docker-entrypoint.sh']
