class FetchBuildCommand
  def run(build)
    parsed_url = URI.parse(build.url)
    if parsed_url.host == 'www.d3planner.com'
      build.source = :d3planner
      build.source_id = URI(build.url).path.gsub(/\//, '')

      # get the build data from d3planner
      build.data = JSON.parse HTTParty.post("http://www.d3planner.com/load?id=#{build.source_id}")
      build.fetched_at = Time.now

      if build.data['profiles'] && build.data['profiles'].length > 0
        build.data = build.data['profiles'].first
      end

      build.data['items'].map do |key, value|
        item_id = value['id']
        item_data = HTTParty.get("https://us.api.battle.net/d3/data/item/#{item_id}?locale=en_US&apikey=ax5ktnsnktc7fzar34d6z3gzkjx4zgqh").body
        item_data = JSON.parse item_data

        build.items << Item.where(blizzard_id: item_id)
                            .first_or_create(blizzard_id: item_id, data: item_data)
      end
    end
    build
  end
end
