json.array!(@builds) do |build|
  json.extract! build, :id, :title, :url, :data
end
