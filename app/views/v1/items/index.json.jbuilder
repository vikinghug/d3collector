json.array!(@items) do |item|
  json.id item.id
  json.data item.data
  json.builds item.builds
  json.count item.builds.count
end
