class Build < ActiveRecord::Base
  belongs_to :user
  has_and_belongs_to_many :items

  validates :url, presence: true
  validates_format_of :url, with: /(d3planner\.com\/[0-9]+|diablofans\.com\/builds\/[0-9]+\-)/i
end
