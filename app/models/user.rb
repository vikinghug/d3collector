class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :omniauthable, :trackable, :omniauth_providers => [:bnet]
  has_many :builds

  class << self
    def from_omniauth(auth)
      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.battletag = auth.info.battletag
        user.blizzard_account_id = auth.info.id
      end
    end
  end
end
