module ItemsHelper
  def items_from_slot(items, slot)
    items.select{|item| item.data['slots'].include?(slot)}
  end
end
