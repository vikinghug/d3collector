require "uri"

module V1
  class BuildsController < ApplicationController
    before_action :set_build, only: [:show, :edit, :update, :destroy]
    skip_before_action :verify_authenticity_token
    respond_to :html, :json

    # GET /builds
    # GET /builds.json
    def index
      @builds = current_user.builds.all
    end

    # GET /builds/1
    # GET /builds/1.json
    def show
    end

    # GET /builds/new
    def new
      @build = Build.new
    end

    # GET /builds/1/edit
    def edit
    end

    # POST /builds
    # POST /builds.json
    def create
      @build = FetchBuildCommand.new.run(Build.new(build_params))
      @build.user = current_user

      respond_to do |format|
        if @build.save
          format.html { redirect_to @build, notice: 'Build was successfully created.' }
          format.json { render json: current_user.builds.all, status: :ok }
        else
          format.html { render :new }
          format.json { render json: @build.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /builds/1
    # PATCH/PUT /builds/1.json
    def update
      respond_to do |format|
        if @build.update(build_params)
          format.html { redirect_to @build, notice: 'Build was successfully updated.' }
          format.json { render :show, status: :ok, location: @build }
        else
          format.html { render :edit }
          format.json { render json: @build.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /builds/1
    # DELETE /builds/1.json
    def destroy
      @build.destroy
      respond_to do |format|
        format.html { redirect_to builds_url, notice: 'Build was successfully destroyed.' }
        format.json { render json: current_user.builds.all, status: :ok }
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_build
      @build = Build.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def build_params
      params.permit(:title, :url)
    end
  end
end
