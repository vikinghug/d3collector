module V1
  class ItemsController < ApplicationController

    def index
      puts '='*20
      puts "Using BNET_KEY: #{ENV['BNET_KEY']}"
      puts "Using BNET_SECRET: #{ENV['BNET_SECRET']}"
      puts '='*20

      @items = Build.all.map(&:items).flatten.uniq
    end

  end
end
