class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def bnet
    @user = User.from_omniauth(request.env["omniauth.auth"])
    sign_in_and_redirect @user
  end

  def after_omniauth_failure_path_for(scope)
    redirect_to '/builds'
  end
end
