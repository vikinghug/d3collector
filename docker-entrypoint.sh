#!/usr/bin/env bash
[[ -z "$RAILS_ENV" ]] && export RAILS_ENV=development

# Symlink the log file to stdout for Docker
echo "⚰  Symlinking to /code/log/$RAILS_ENV.log"
ln -sf /dev/stdout /code/log/$RAILS_ENV.log

if [ -e ".env" ]; then
  # Load any local ENV vars
  echo "⚰  Loading local environment variables..."
  source .env
fi

if [ "$RAILS_ENV" = '' -o "$RAILS_ENV" = 'development' ]; then
  echo "🤖  Databasing the rakes..."
  bundle exec rake db:create db:migrate
fi

echo "🤖  Starting the server..."
rm -rf /code/tmp/pids/*.pid
bundle exec unicorn --port=3000
