require 'rails_helper'

describe Build do

  context "when creating" do

    context "with a d3planner url" do
      let(:build) { FactoryGirl.build :d3planner_build }

      it "is valid" do
        expect(build).to be_valid
      end
    end

    context "with a diablofans url" do
      let(:build) { FactoryGirl.build :diablofans_build }

      it "is valid" do
        expect(build).to be_valid
      end
    end

    context "without a url" do
      let(:build) { FactoryGirl.build :invalid_build }

      it "is valid" do
        expect(build).to_not be_valid
      end
    end
  end
end
