require 'faker'

FactoryGirl.define do
  factory :build do |f|
    f.title { Faker::Company.buzzword }
    f.url do
      %w(
        http://www.diablofans.com/builds/75813-barb-sup-no-anciens
        http://www.diablofans.com/builds/75758-derps-updated-gen-monk-2-0-solo-86
        http://www.diablofans.com/builds/75732-2-4-sages-zunis-voos-deaths-breath-farmer
        http://www.d3planner.com/484546651
        http://www.d3planner.com/197330407
        http://www.d3planner.com/356123055
      ).sample
    end

    factory :invalid_build do |f|
      f.url { nil }
    end

    factory :d3planner_build do |f|
      f.url { 'http://www.d3planner.com/484546651' }
    end

    factory :diablofans_build do |f|
      f.url { 'http://www.diablofans.com/builds/75732-2-4-sages-zunis-voos-deaths-breath-farmer' }
    end
  end
end
