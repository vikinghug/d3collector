import _               from 'lodash';
import cx              from 'classnames';
import React           from 'react';
import request         from 'reqwest';
import connectToStores from 'alt/utils/connectToStores';

import ListStore      from './ListStore';
import ListActions    from './ListActions';

import AddBuild         from './AddBuild';

class List extends React.Component {
  static getStores () {
    return [ListStore];
  }

  static getPropsFromStores () {
    return _.merge(
      ListStore.getState()
    );
  }

  componentWillMount () {
    request({
      url: `/v1/builds.json`
    })
    .then((response) => {
      ListActions.setBuilds(response);
    });
  }

  handleDeleteBuildClick (id) {
    request({
      url: `/v1/builds/${id}.json`,
      method: 'delete'
    })
    .then((response) => {
      ListActions.setBuilds(response);
    });
  }

  render () {
    let classes = cx('list__wrapper');

    let addingBuildItem;
    if (this.props.addingBuild) {
      let portraitUrl = `/assets/images/gem_18.png`;

      addingBuildItem = (
        <li className='build-row importing-build'>
          <img className='spinner-icon' src={portraitUrl} />
          <span className='build-title'>Importing build...</span>
        </li>
      );
    }

    let buildsList = this.props.builds.map((build, key) => {
      let data = build.data;
      let portraitUrl = `http://us.media.blizzard.com/d3/icons/portraits/42/${data.class}_${data.gender}.png`;

      return (
        <li key={key} className='build-row'>
          <img className='portrait' src={portraitUrl} />
          <span className='build-title'>{build.title}</span>
          <div className='icon-frame'
            onClick={this.handleDeleteBuildClick.bind(this, build.id)}
          >
            <i className='icon-plus' />
          </div>
        </li>
      );
    });

    return (
      <div className={classes}>
        <div className='header'>
          <h2 className='title'>List</h2>
        </div>
        <ul>
          {buildsList}
          {addingBuildItem}
        </ul>
        <AddBuild show={this.props.showAddBuild} />
      </div>
    );
  }
}

export default connectToStores(List);
