import cx              from 'classnames';
import React           from 'react';
import request         from 'reqwest';

import ListActions     from './ListActions';

export default class AddBuild extends React.Component {

  constructor (props) {
    super(props);
    this.state = {title: '', url: ''};
  }

  componentDidMount () {
    document.addEventListener('keyup', this.handleEscapeKey);
  }

  componentWillUnmount () {
    document.removeEventListener('keyup', this.handleEscapeKey);
  }

  handleEscapeKey (event) {
    if (event.keyCode === 27) {
      ListActions.showAddBuild(false);
    }
  }

  handleTitleChange (e) {
    this.setState({title: e.target.value});
  }

  handleUrlChange (e) {
    this.setState({url: e.target.value});
  }

  handleBuildSubmit (e) {
    e.preventDefault();
    ListActions.setAddingBuild(true);

    request({
      url: `/v1/builds.json`,
      method: 'post',
      data: {
        title: this.state.title.trim(),
        url: this.state.url.trim(),
      }
    })
    .then((response) => {
      this.state = {title: '', url: ''};
      ListActions.setBuilds(response);
    })
    .catch((error) => {
      this.setState({errorText: error.responseText});
      ListActions.setAddingBuild(false);
    });
  }

  handleAddBuildClick (event) {
    console.log(this.props.show);
    event.preventDefault();
    ListActions.showAddBuild(!this.props.show);
  }

  render () {
    let classes = cx('add-build__wrapper');

    if (!this.props.show) {
      return (
        <div className={classes}>
          <button onClick={this.handleAddBuildClick.bind(this)}>
            Add Build
          </button>
        </div>
      );
    }

    let errorMessage;
    if (this.state.errorText) {
      errorMessage = (
        <div className='alert'>
          {this.state.errorText}
        </div>
      );
    }

    return (
      <div className={classes}>
        {errorMessage}
        <form onSubmit={this.handleBuildSubmit.bind(this)}>
          <input placeholder='Title'
            autoFocus='true'
            onChange={this.handleTitleChange.bind(this)}
            value={this.state.title}
          />
          <input placeholder='URL'
            onChange={this.handleUrlChange.bind(this)}
            value={this.state.url}
          />

          <button>Add Build</button>
        </form>
        <button
          className='cancel'
          onClick={this.handleAddBuildClick.bind(this)}
        >
          Cancel
        </button>
      </div>
    );
  }
}
