import alt  from '../../../lib/alt';

class ListActions {
  constructor () {
    this.generateActions('setBuilds', 'showAddBuild', 'setAddingBuild');
  }
}

export default alt.createActions(ListActions);
