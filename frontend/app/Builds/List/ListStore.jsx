import alt  from '../../../lib/alt';
import ListActions from './ListActions';

// Store
class ListStore {
  constructor () {
    this.bindActions(ListActions);

    this.state = {
      builds: [],
      showAddBuild: false,
      addingBuild: true
    };
  }

  onSetBuilds (builds) {
    this.setState({builds: builds, addingBuild: false});
  }

  onShowAddBuild (show) {
    this.setState({showAddBuild: show});
  }

  onSetAddingBuild (state) {
    this.setState({addingBuild: state});
  }
}

export default alt.createStore(ListStore, 'ListStore');
