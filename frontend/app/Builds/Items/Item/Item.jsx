import cx              from 'classnames';
import React           from 'react';

export default class Item extends React.Component {
  handleTooltipClick (event) {
    event.preventDefault();
  }

  render () {
    let classes = cx('item__wrapper');

    let itemsList = this.props.items.map((item, key) => {
      let data = item.data;
      let linkClasses = cx('tooltip-link', data.displayColor);

      return (
        <li key={key}>
          <div className={`tooltip-background ${data.displayColor}`} />
          <a
          href={`http://us.battle.net/d3/en/${item.data.tooltipParams}`}
          className={linkClasses}
          onClick={this.handleTooltipClick}>
            <div
            className='item-image'
            style={{
              backgroundImage: `url('http://us.media.blizzard.com/d3/icons/items/large/${data.icon}.png')`
            }} />
            <span className='item-title'>{data.name}</span>
          </a>
          <div className='build-count'>{item.count}</div>
          <ul className='builds-tooltip'>
            {item.builds.map((build, bkey) => {
              return (
                <li key={bkey}>
                  <a href={build.url}>{build.title}</a>
                </li>
              );
            })}
          </ul>
        </li>
      );
    });

    return (
      <li className={classes}>
        <h3>{this.props.title}</h3>
        <ul>
          {itemsList}
        </ul>
      </li>
    );
  }
}
