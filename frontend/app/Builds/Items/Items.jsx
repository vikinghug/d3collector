import _               from 'lodash';
import cx              from 'classnames';
import React           from 'react';
import request         from 'reqwest';
import connectToStores from 'alt/utils/connectToStores';

import ItemsStore      from './ItemsStore';
import ItemsActions    from './ItemsActions';

import Item            from './Item/Item';

class Items extends React.Component {
  static getStores () {
    return [ItemsStore];
  }

  static getPropsFromStores () {
    return _.merge(
      ItemsStore.getState()
    );
  }

  componentWillMount () {
    request({
      url: `/v1/items.json`
    })
    .then((response) => {
      ItemsActions.setItems(response);
    });
  }

  render () {
    let classes = cx('items__wrapper');

    return (
      <div className={classes}>
        <div className='max-row'>
          <div className='small-12 columns'>
            <h2 className='title'>Items</h2>
          </div>
          <div className='small-12 columns'>
            <ul className='small-block-grid-1 medium-block-grid-3 large-block-grid-4'>
              <Item title='Head'      items={this.props.head} />
              <Item title='Shoulders' items={this.props.shoulders} />
              <Item title='Chest'     items={this.props.chest} />
              <Item title='Belts'     items={this.props.belts} />
              <Item title='Gloves'    items={this.props.gloves} />
              <Item title='Bracers'   items={this.props.bracers} />
              <Item title='Legs'      items={this.props.legs} />
              <Item title='Boots'     items={this.props.boots} />
              <Item title='Amulets'   items={this.props.amulets} />
              <Item title='Rings'     items={this.props.rings} />
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default connectToStores(Items);
