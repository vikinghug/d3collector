import alt  from '../../../lib/alt';
import ItemsActions from './ItemsActions';

// Store
class ItemsStore {
  constructor () {
    this.bindActions(ItemsActions);

    this.state = {
      head: [],
      shoulders: [],
      chest: [],
      belts: [],
      gloves: [],
      bracers: [],
      legs: [],
      boots: [],
      amulets: [],
      rings: [],
    };
  }

  onSetItems (items) {
    this.setState(items);
  }
}

export default alt.createStore(ItemsStore, 'ItemsStore');
