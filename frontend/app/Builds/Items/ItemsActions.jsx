import _    from 'lodash';
import alt  from '../../../lib/alt';

function findBySlot (items, slot) {
  return _.select(items, (item) => {
    return _.includes(item.data.slots, slot);
  });
}

class ItemsActions {
  constructor () {
    this.generateActions();
  }

  setItems (items) {
    return {
      head: findBySlot(items, 'head'),
      shoulders: findBySlot(items, 'shoulder'),
      chest: findBySlot(items, 'chest'),
      belts: findBySlot(items, 'waist'),
      gloves: findBySlot(items, 'hands'),
      bracers: findBySlot(items, 'bracers'),
      legs: findBySlot(items, 'legs'),
      boots: findBySlot(items, 'feet'),
      amulets: findBySlot(items, 'neck'),
      rings: findBySlot(items, 'left-finger')
    };
  }
}

export default alt.createActions(ItemsActions);
