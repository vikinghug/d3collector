import cx              from 'classnames';
import React           from 'react';

import List            from './List/List';
import Items           from './Items/Items';

export default class Builds extends React.Component {
  render () {
    let classes = cx('builds__wrapper');

    return (
      <div className={classes}>
        <div className='sidebar'>
          <List />
        </div>
        <div className='content'>
          <Items />
        </div>
      </div>
    );
  }
}
