import cx              from 'classnames';
import React           from 'react';

export default class Home extends React.Component {
  render () {
    let classes = cx('home__wrapper');

    return (
      <div className={classes}>
        <div className='row'>
          <div className='small-12 small-centered columns text-center'>
            <h1>d3collector</h1>
          </div>
        </div>
        <div className='row'>
          <div className='small-6 small-centered columns text-center'>
            <div className='login-panel'>
              <h2>
                Login with
              </h2>
              <img
              />
              <div className='row'>
                <div className='small-12 columns'>
                  <a className='login-button' href='/users/auth/bnet'>
                    <span className='battlenet-logo'
                    style={{
                      backgroundImage: `url('http://us.battle.net/static/local-common/images/logos/bnet-default.png')`,
                      backgroundSize: 'contain',
                      width: 100
                    }}
                    >Battle.net</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
