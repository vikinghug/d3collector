/* eslint no-unused-vars: [2, {"varsIgnorePattern": "^React"}] */
import {IndexRoute, Router, Route} from 'react-router';
import React           from 'react';
import history         from '../lib/history';

import Home            from './Home/Home';
import Builds          from './Builds/Builds';
import Layout          from './Layout';

// declare our routes and their hierarchy
let routes = (
  <Route path="/" component={Layout}>
    <Route path='builds' component={Builds} />

    <IndexRoute component={Home} />
  </Route>
);

export default <Router history={history} routes={routes} />;
