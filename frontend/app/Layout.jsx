import cx              from 'classnames';
import React           from 'react';

export default class Layout extends React.Component {
  render () {
    let classes = cx('layout__wrapper');

    return (
      <div className={classes}>
        {this.props.children}
      </div>
    );
  }
}

