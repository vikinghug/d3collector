Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }

  devise_scope :user do
    get "/users/sign_out" => "devise/sessions#destroy"
  end

  namespace 'v1' do
    resources :builds
    resources :items
  end

  get "/*path" => 'frontend#index'
  root to: 'frontend#index'
end
