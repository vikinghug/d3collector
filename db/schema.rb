# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160226013021) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "builds", force: :cascade do |t|
    t.string   "title"
    t.string   "url"
    t.string   "source"
    t.string   "source_id"
    t.json     "data"
    t.datetime "fetched_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  add_index "builds", ["user_id"], name: "index_builds_on_user_id", using: :btree

  create_table "builds_items", force: :cascade do |t|
    t.integer "build_id"
    t.integer "item_id"
  end

  add_index "builds_items", ["build_id"], name: "index_builds_items_on_build_id", using: :btree
  add_index "builds_items", ["item_id"], name: "index_builds_items_on_item_id", using: :btree

  create_table "items", force: :cascade do |t|
    t.string   "blizzard_id"
    t.json     "data"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer  "sign_in_count",       default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "provider",                        null: false
    t.string   "uid"
    t.string   "blizzard_account_id"
    t.string   "battletag"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "build_id"
  end

  add_index "users", ["build_id"], name: "index_users_on_build_id", using: :btree

  add_foreign_key "builds", "users"
  add_foreign_key "users", "builds"
end
