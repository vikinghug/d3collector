class AddUserRefToBuilds < ActiveRecord::Migration
  def change
    add_reference :builds, :user, index: true, foreign_key: true
    add_reference :users, :build, index: true, foreign_key: true
  end
end
