class CreateItemsAndJoinTable < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :blizzard_id
      t.json :data

      t.timestamps null: false
    end

    create_table :builds_items do |t|
      t.belongs_to :build, index: true
      t.belongs_to :item, index: true
    end
  end
end
