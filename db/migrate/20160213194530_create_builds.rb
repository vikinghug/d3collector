class CreateBuilds < ActiveRecord::Migration
  def change
    create_table :builds do |t|
      t.string :title
      t.string :url

      t.string :source
      t.string :source_id

      t.json :data

      t.datetime :fetched_at

      t.timestamps null: false
    end
  end
end
