/* global environment, path, config */
global.config = {};
config.PROJECT_ROOT = path.join(__dirname, '..');
config.DIST = path.join(config.PROJECT_ROOT, 'app', 'assets', '.build');
config.STANDALONE = process.env.STANDALONE === 'true';

var envAssets = require('./environment/' + environment);

config.VENDOR_ASSETS = envAssets.vendor;

config.assets = {
  src: [
    path.join(config.PROJECT_ROOT, '/frontend/**/*.{png,jpg,mp4,eot,svg,ttf,woff,html,ico,svg}'),
    path.join(config.PROJECT_ROOT, '/frontend/CNAME')
  ],
  dest: config.DIST,
  base: 'frontend/assets'
};

config.assetsVendor = {
  src: config.VENDOR_ASSETS,
  dest: config.DIST
};

config.build = {
  src: config.PROJECT_ROOT + '',
  dest: config.DIST
};

config.clean = {
  src: [
    path.join(config.DIST),
  ]
};

config.js = {
  src: path.join(config.PROJECT_ROOT, '/frontend/app/index.{jsx,js}'),
  dest: path.join(config.DIST),
  webpackOptions: require(config.PROJECT_ROOT + '/webpack.config.js'),
};

config.lintJs = {
  src: [
    path.join(config.PROJECT_ROOT, '/frontend/**/*.{jsx,js}'),
    path.join(config.PROJECT_ROOT, '/gulp/**/*.js'),
    path.join(config.PROJECT_ROOT, '/gulpfile.js'),
    path.join(config.PROJECT_ROOT, '/webpack.config.js')
  ],
};

config.sass = {
  src: path.join(config.PROJECT_ROOT, '/frontend/app/index.scss'),
  dest: path.join(config.DIST),
  filter: '**/*.css',

  inject: {
    src: [path.join(config.PROJECT_ROOT, '/frontend/app/**/*.scss')],
    options: {
      transform: function (filepath) {
        return '@import "' + filepath + '";';
      },
      starttag: '// inject:scss',
      endtag: '// endinject',
      addRootSlash: false
    }
  },

  options: {
    style: 'expanded',
    errLogToConsole: true,
    includePaths: [
      path.join(config.PROJECT_ROOT, '/frontend/app'),
      path.join(config.PROJECT_ROOT, '/frontend/assets/css'),
      path.join(config.PROJECT_ROOT, '/bower_components/foundation/scss')
    //   require('node-bourbon').includePaths
    ]
  }
};

config.lintSass = {
  src: [
    path.join(config.PROJECT_ROOT, '/frontend/**/*.scss'),
    path.join('!', config.PROJECT_ROOT, '/frontend/assets/css/_reset.scss'),
  ],
  options: require(`${config.PROJECT_ROOT}/.sasslint.js`),
};

config.watch = {
  assets: {
    task: 'assets',
    src: config.assets.src
  },
  assetsVendor: {
    task: 'assets:vendor',
    src: config.assetsVendor
  },
  jade: {
    task: 'jade',
    src: path.join(config.PROJECT_ROOT, '/frontend/**/*.jade')
  },
  js: {
    task: 'js',
    src: path.join(config.PROJECT_ROOT, '/frontend/**/*.{jsx,js}')
  },
  lintJs: {
    task: 'lint-js',
    src: config.lintJs.src
  },
  sass: {
    task: 'sass',
    src: path.join(config.PROJECT_ROOT, '/frontend/**/*.scss')
  },
  lintSass: {
    task: 'lint-sass',
    src: path.join(config.PROJECT_ROOT, '/frontend/**/*.scss')
  }
};

module.exports = config;
